# Welcome to RethiSoft contributing guide

**Thank you for your interest in contributing to our project!**

This guide will explain you:

- How to start contributing to RethiSoft’s projects,
- How to work with _branches_ in our repositories,
- How to use the _issue tracker_,
- What are RethiSoft’s _code requirements_ and _goals_.

## Just starting out

### Git

Git is the core of any Open Source project. It helps mainly in
collaborative code development and version management. If you are
unfamiliar, please checkout [this
guide](https://github.com/progit/progit2/releases/download/2.1.331/progit.pdf).

## Branches

RethiSoft respects Git Flow Branch Strategy:

- **`main`** — stable, production code, **ready to deploy**, tagged with
  _release version_;
- **`develop`** — the main environment of **development**, derived from
  `main`; it consists of:
  - commits with small changes in the code,
  - squashed and merged commits from `feature` branch;
- **`feature/feature-name`** — development code of a complex **feature**
  (not suited in one commit); started from `develop`, squashed and merged
  to `develop`;
- **`release/*`** — _preparation_ to **release**; forked from
  `develop`, squashed and merged to `main` and `develop`;
- **`hotfix/#number`** — fix for an **urgent issue** with `hotfix` tag;
  started from `main`, squashed and merged to `main` and `develop`.

Read more about Git Flow
[here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

## Merge Request Guidelines

- Commits:
  - Try to keep commit messages _brief_ and informative,
  - In commit messages, convey the _reason_ of commit rather than
    introduced changes,
  - Remove unnecessary commits and _squash_ commits together when
    appropriate,
  - Please, ensure that your code meets [our requirements](code_style.md)
    before committing,
- Merge workflow:
  - Respecting GitFlow workflow, for each feature create a relevant branch
    and use it as development environment,
  - Pull commits that are sure (do not require later rebasing),
  - Merge feature branches to `develop` only when they are _tested_ well
    (you can create a draft merge request to ask for help in testing).

## Tasks

For a complete list of current tasks see the **issue tracker** of relevant
repository on [Gitlab](https://gitlab.com/rethisoft).

## Contact

For more information, contact [_Adam
Maleszka_](https://gitlab.com/maleszka) (owner) via email: <adam_maleszka@aol.com>.
