<pre>
    ____          __     __      _    _____           ____   __
   / __ \  ___   / /_   / /_    (_)  / ___/  ____    / __/  / /_
  / /_/ / / _ \ / __/  / __ \  / /   \__ \  / __ \  / /_   / __/
 / _, _/ /  __// /_   / / / / / /   ___/ / / /_/ / / __/  / /_
/_/ |_|  \___/ \__/  /_/ /_/ /_/   /____/  \____/ /_/     \__/


<em>Rethinking modern software as the place for performance and optimization</em>
</pre>

# Welcome to the RethiSoft!

There is only one group and one goal. It is our pleasure to welcome You to
the RethiSoft’s open documents collection.

## Who we are?

We are a group of **passionate** founded to change the approach to
contemporary programs. Our overarching objective is to create _efficient_
programs that don’t do much but do them **the best they can**. We do not
tolerate corporate crap. Instead, we promote _privacy_ and _performance_.

## Motivation

Undoubtedly, everyone could confirm that corporations dominate the
software market. Especially popular are getting heavy “all-in-one” suites,
expensive “professional” tools, and slow browser applications.

There is a certain lack of ethics in the whole thing, of remembering that
the software market _is not just a business_. That’s why we founded
RethiSoft, which intends to _change the image of modern software_ by
ensuring **privacy** through **Open Source** and refining the code
_performance_ as best as possible.

## Our values

- **Performance** — the main indicator of code quality. In terms of it, we
  try to use the most _efficient algorithms_ possible or invent our own if
  necessary.
- **Privacy** — we consider that the downloaded program becomes the
  _property of the user_. Therefore, all information collected by the
  program should also be his property under _respect for his privacy_.
- **Simplicity** — it is straightforward to implement many features but
  challenging to _do a few well_.

## How to get started?

- If you would be wiling to **get involved**, please read
  [this](CONTRIBUTING.md) first.
- A list of **all projects** currently in development is available on
  [GitLab](https://gitlab.com/rethisoft).

## Contact

For more information, contact [me](https://gitlab.com/maleszka) (owner)
via email: <adam_maleszka@aol.com>.
