# Code Writing Agreements

**Thank you for your interest in contributing to our project.**

This file is an extension of
[CONTRIBUTING.md](https://gitlab.com/rethisoft/documents/-/blob/main/CONTRIBUTING.md).
Make sure to read it first, before this one!

This document will explain to you:

- What code style you should use while contributing to our projects,
- Which utility tools do we use,
- How you should use third-party software configuration files.

## Code style

Code style is a set of rules, that we use when writing code. Setting rigid
standards makes it easier to find your way around the code and thus avoid
mistakes.

Accuracy and popularity led us to choose the [Google's
style](https://google.github.io/styleguide/cppguide.html). It is
applicable to all RethiSoft’s projects using C++. You should keep that in
mind to ensure that your merge requests are not denied.

## Recommended utility tools

### Cpplint — style linter

Cpplint is a command line tool that checks C++ files for code-style
issues, following mentioned before Google's c++ style guide. You can get
cpplint [here](https://github.com/cpplint/cpplint).

### Clang-format — code formatter

Clang-format is a tool that automatically formats your code so you don’t
have to worry about code-style errors.

It is a part of [Clang project](https://clang.llvm.org/).

## Third-party software configuration files

In general, you're free to use any third-party tool you want. However,
keep in mind that including configuration files of utilities not mentioned
above is not allowed.

For example, if you use `ccls` that needs `.ccls` and
`compile_commands.json` files, don't include them in your merge requests.

## See also

- [CONTRIBUTING.md](CONTRIBUTING.md) — brief guide to contributing
